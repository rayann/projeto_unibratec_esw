package com.daideia.contatos.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

public class SettingsPreferences {

    private final int ENGLISH = 1;
    private final int PORTUGUESE = 2;
    private final String ENGLISH_ = "ENGLISH";
    private final String PORTUGUESE_ = "PORTUGUESE";

    // Shared Preferences
    private SharedPreferences pref;

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // Context
    private Context _context;

    public SettingsPreferences(Context context) {
        this._context = context;
        getSharedPreferences();
    }

    private void getSharedPreferences() {
        pref = _context.getSharedPreferences("LanguagePref", Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public int getLanguage() {
        String l = pref.getString("language", ENGLISH_);
        return (l.equals(ENGLISH_)) ? ENGLISH : PORTUGUESE;
    }

    public void setLanguage(Integer language) {
        editor.putString("language", (language == ENGLISH) ? ENGLISH_ : PORTUGUESE_);
        editor.commit();
        setPhoneLanguage(language);
    }

    public void setPhoneLanguage(Integer language) {
        String lang = (language == ENGLISH) ? "en" : "pt";
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = _context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(config.locale);
        } else {
            config.locale = locale;
            _context.getResources().updateConfiguration(config, _context.getResources().getDisplayMetrics());
        }
    }
}