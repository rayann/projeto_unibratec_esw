package com.daideia.contatos.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.daideia.contatos.R;
import com.daideia.contatos.database.ContatosDB;
import com.daideia.contatos.log.Logger;
import com.daideia.contatos.models.Contact;
import com.daideia.contatos.utils.ContactsAdapter;
import com.daideia.contatos.utils.Message;
import com.daideia.contatos.utils.SettingsPreferences;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    // UI references.
    private View mMainView, mProgressView;
    private ListView listView;

    // Services references.
    private SettingsPreferences settingsPreferences = null;
    private int _LANGUAGE = -1;
    private int selectedLang = -1;
    private LoadContactsTask mTask = null;
    private ContatosDB contatosDB = null;

    private List<Contact> mListContacts = new ArrayList<>();

    private void getLanguage() {
        settingsPreferences = new SettingsPreferences(getApplicationContext());
        _LANGUAGE = selectedLang = settingsPreferences.getLanguage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLanguage();
        settingsPreferences.setPhoneLanguage(_LANGUAGE);
        setContentView(R.layout.activity_main);

        mMainView = findViewById(R.id.main_form);
        mProgressView = findViewById(R.id.main_progress);

        listView = findViewById(R.id.contacts_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact contact = mListContacts.get(position);
                String number = null;
                if (contact.getPhone() != null && !contact.getPhone().isEmpty()) {
                    number = contact.getPhone();
                } else if (contact.getCellphone() != null && !contact.getCellphone().isEmpty()) {
                    number = contact.getCellphone();
                } else {
                    Message.showLongMessage(getApplicationContext(), getString(R.string.msg_no_contacts));
                }

                if (number != null) {
                    Uri uri = Uri.parse("tel:" + number);
                    startActivity(new Intent(Intent.ACTION_DIAL, uri));
                }
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getApplicationContext(), FormContactActivity.class);
                it.putExtra("EXTRA_CONTACT", mListContacts.get(position));
                startActivity(it);
                return true;
            }
        });

        showProgress(true);

        contatosDB = new ContatosDB(getApplicationContext());

        if (savedInstanceState != null) {
            mListContacts = (List<Contact>) savedInstanceState.getSerializable("EXTRA_CONTACTS");
            listView.setAdapter(new ContactsAdapter(getApplicationContext(), mListContacts));
            showProgress(false);
        } else {
            mTask = new LoadContactsTask();
            mTask.execute();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTask = new LoadContactsTask();
        mTask.execute();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("EXTRA_CONTACTS", (Serializable) mListContacts);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_settings, menu);
        inflater.inflate(R.menu.menu_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Action back in bar home/up
        switch (item.getItemId()) {
            case R.id.action_settings:
                changeLanguage();
                break;
            case R.id.action_refresh:
                new LoadContactsTask().execute();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mMainView.setVisibility(show ? View.GONE : View.VISIBLE);
            mMainView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mMainView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mMainView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public class LoadContactsTask extends AsyncTask<Void, Void, List<Contact>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(true);
        }

        protected List<Contact> doInBackground(Void... params) {

            try {
                return contatosDB.getContacts();
            } catch (Exception e) {
                Logger.log(Logger.ERROR, e.getMessage());
            }
            return null;
        }

        protected void onPostExecute(final List<Contact> result) {
            mTask = null;
            showProgress(false);

            if (result != null) {
                mListContacts = result;
                listView.setAdapter(new ContactsAdapter(getApplicationContext(), mListContacts));
            }
        }

        protected void onCancelled() {
            mTask = null;
            showProgress(false);
        }
    }

    public void newContact(View v) {
        startActivity(new Intent(this, FormContactActivity.class));
    }

    public void changeLanguage() {
        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int paramInt) {
                //Alterar o idioma do app
                settingsPreferences.setLanguage(selectedLang);
                getLanguage();
                dialog.dismiss();
                Message.showLongMessage(getApplicationContext(), getString(R.string.language_selected));
                recreate();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.select_language));

        final CharSequence[] items = {getString(R.string.english), getString(R.string.portuguese)};
        int lang = _LANGUAGE - 1;
        builder.setSingleChoiceItems(items, lang,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        selectedLang = item + 1;
                    }
                });
        builder.setPositiveButton(getString(R.string.save), listener);
        builder.show();

        /*Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_languages);
        dialog.setTitle("This is my custom dialog box");
        dialog.setCancelable(true);
        // there are a lot of settings, for dialog, check them all out!
        // set up radiobutton
        RadioButton rd1 = (RadioButton) dialog.findViewById(R.id.rdPortuguese);
        RadioButton rd2 = (RadioButton) dialog.findViewById(R.id.rdEnglish);

        // now that the dialog is set up, it's time to show it
        dialog.show();*/
    }
}
