package com.daideia.contatos.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.daideia.contatos.database.dao.ContactDAO;
import com.daideia.contatos.models.Contact;

import java.util.List;

public class ContatosDB {
    private PersistenceHelper mHelper;
    private static ContatosDB instance;

    public static ContatosDB getInstance(Context context) {
        if (instance == null)
            instance = new ContatosDB(context);
        return instance;
    }

    public static void closeConnection(SQLiteDatabase database) {
        if (database != null && database.isOpen()) {
            database.close();
        }
    }

    public ContatosDB(Context context) {
        mHelper = new PersistenceHelper(context);
    }

    /* Contacts functions */
    public Contact getContactById(Integer contactId) {
        ContactDAO contactDAO = new ContactDAO(mHelper.getReadableDatabase());
        return contactDAO.getContactById(contactId);
    }

    public List<Contact> getContacts() {
        ContactDAO contactDAO = new ContactDAO(mHelper.getReadableDatabase());
        return contactDAO.getContacts();
    }

    public boolean addContact(Contact contact) {
        ContactDAO contactDAO = new ContactDAO(mHelper.getWritableDatabase());
        return contactDAO.insert(contact);
    }

    public boolean updateContact(Contact contact) {
        ContactDAO contactDAO = new ContactDAO(mHelper.getWritableDatabase());
        return contactDAO.update(contact);
    }

    public boolean deleteContactById(Integer contactId) {
        ContactDAO contactDAO = new ContactDAO(mHelper.getWritableDatabase());
        return contactDAO.delete(contactId);
    }
    /* END Contacts functions */
}