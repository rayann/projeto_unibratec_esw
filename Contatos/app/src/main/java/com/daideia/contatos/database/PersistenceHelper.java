package com.daideia.contatos.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.daideia.contatos.database.tables.ContactsTB;

public class PersistenceHelper extends SQLiteOpenHelper {
    private static String DB_NAME = "ContactsDB";
    private static int VERSION = 1;

    public PersistenceHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ContactsTB.createTableContactsQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(ContactsTB.deleteTableContactsQuery);
        onCreate(db);
    }
}

