package com.daideia.contatos.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.daideia.contatos.database.ContatosDB;
import com.daideia.contatos.database.tables.ContactsTB;
import com.daideia.contatos.log.Logger;
import com.daideia.contatos.models.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactDAO {

    private SQLiteDatabase _database;

    public ContactDAO(SQLiteDatabase database) {
        this._database = database;
    }

    private ContentValues makeContentValues(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(ContactsTB.NAME, contact.getName());
        values.put(ContactsTB.PHONE, contact.getPhone());
        values.put(ContactsTB.CELLPHONE, contact.getCellphone());
        values.put(ContactsTB.EMAIL, contact.getEmail());
        return values;
    }

    private Contact makeContact(Cursor cursor) {
        return new Contact(
                cursor.getInt(cursor.getColumnIndex(ContactsTB.ID)),
                cursor.getString(cursor.getColumnIndex(ContactsTB.NAME)),
                cursor.getString(cursor.getColumnIndex(ContactsTB.PHONE)),
                cursor.getString(cursor.getColumnIndex(ContactsTB.CELLPHONE)),
                cursor.getString(cursor.getColumnIndex(ContactsTB.EMAIL))

        );
    }

    public Contact getContactById(Integer contactId) {
        String query = "SELECT * FROM " + ContactsTB.TABLE_NAME + " WHERE " + ContactsTB.ID + " = " + contactId + ";";
        Cursor cursor = this._database.rawQuery(query, null);
        Contact contact = (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) ? makeContact(cursor) : null;
        cursor.close();
        ContatosDB.closeConnection(this._database);
        return contact;
    }

    public List<Contact> getContacts() {
        List<Contact> contacts = new ArrayList<>();
        String query = "SELECT * FROM " + ContactsTB.TABLE_NAME + ";";
        Cursor cursor = this._database.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            do {
                contacts.add(makeContact(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        ContatosDB.closeConnection(this._database);
        return contacts;
    }

    public boolean insert(Contact contact) {
        try {
            ContentValues values = makeContentValues(contact);
            this._database.insert(ContactsTB.TABLE_NAME, null, values);
            ContatosDB.closeConnection(this._database);
            return true;
        } catch (Exception e) {
            Logger.log(Logger.ERROR, e.getMessage());
        }
        return false;
    }

    public boolean update(Contact contact) {
        try {
            ContentValues values = makeContentValues(contact);
            String query = ContactsTB.ID + " = " + contact.getId() + ";";
            this._database.update(ContactsTB.TABLE_NAME, values, query, null);
            ContatosDB.closeConnection(this._database);
            return true;
        } catch (Exception e) {
            Logger.log(Logger.ERROR, e.getMessage());
        }
        return false;
    }

    public boolean delete(Integer contactId) {
        try {
            String query = ContactsTB.ID + " = " + contactId + ";";
            this._database.delete(ContactsTB.TABLE_NAME, query, null);
            ContatosDB.closeConnection(this._database);
            return true;
        } catch (Exception e) {
            Logger.log(Logger.ERROR, e.getMessage());
        }
        return false;
    }
}
