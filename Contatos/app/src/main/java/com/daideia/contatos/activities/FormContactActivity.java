package com.daideia.contatos.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.daideia.contatos.R;
import com.daideia.contatos.database.ContatosDB;
import com.daideia.contatos.models.Contact;
import com.daideia.contatos.utils.Message;

public class FormContactActivity extends AppCompatActivity {

    // UI references.
    private View mFormView;
    private EditText mTxtName, mTxtPhone, mTxtCellphone, mTxtEmail;
    private MenuItem mDelete;

    // Services references.
    private ContatosDB contatosDB = null;

    Contact mContactLoaded = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_contact);

        mFormView = findViewById(R.id.add_contact_form);

        mTxtName = findViewById(R.id.txtFormName);
        mTxtPhone = findViewById(R.id.txtFormPhone);
        mTxtCellphone = findViewById(R.id.txtFormCellphone);
        mTxtEmail = findViewById(R.id.txtFormEmail);

        contatosDB = new ContatosDB(getApplicationContext());

        if (savedInstanceState != null) {
            if (savedInstanceState.getSerializable("EXTRA_CONTACT") != null) {
                mContactLoaded = (Contact) savedInstanceState.getSerializable("EXTRA_CONTACT");
            }
        } else {
            Intent it = getIntent();
            if (it.hasExtra("EXTRA_CONTACT")) {
                mContactLoaded = (Contact) it.getSerializableExtra("EXTRA_CONTACT");
                if (mContactLoaded == null) {
                    finish();
                }
            }
        }

        if (mContactLoaded != null) {
            setTitle(R.string.activity_form_upd);
            mTxtName.setText(mContactLoaded.getName());
            mTxtPhone.setText(mContactLoaded.getPhone());
            mTxtCellphone.setText(mContactLoaded.getCellphone());
            mTxtEmail.setText(mContactLoaded.getEmail());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mContactLoaded != null) {
            setTitle(R.string.activity_form_upd);
            outState.putSerializable("EXTRA_CONTACT", mContactLoaded);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delete, menu);
        if (mContactLoaded != null) {
            mDelete = menu.findItem(R.id.action_delete);
            mDelete.setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Action back in bar home/up
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_delete:
                if (mDelete != null) mDelete.setVisible(false);
                removeContact();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void saveUpdateContact(View v) {
        Contact contact;
        if (mContactLoaded == null) {
            contact = new Contact(
                    mTxtName.getText().toString(),
                    mTxtPhone.getText().toString(),
                    mTxtCellphone.getText().toString(),
                    mTxtEmail.getText().toString()
            );
        } else {
            contact = mContactLoaded;
            contact.setName(mTxtName.getText().toString());
            contact.setPhone(mTxtPhone.getText().toString());
            contact.setCellphone(mTxtCellphone.getText().toString());
            contact.setEmail(mTxtEmail.getText().toString());
        }

        boolean hasSaved = false;
        StringBuilder message = new StringBuilder();

        if (checkFields(contact)) {
            message.append(getString(R.string.msg_check_fields_form));
        } else {
            if (mContactLoaded == null) {
                message.append(getString(R.string.msg_contact_saved));
                hasSaved = contatosDB.addContact(contact);
                if (!hasSaved) {
                    message.append(getString(R.string.msg_contact_not_saved));
                }
            } else {
                message.append(getString(R.string.msg_contact_updated));
                hasSaved = contatosDB.updateContact(contact);
                if (!hasSaved) {
                    message = new StringBuilder();
                    message.append(getString(R.string.msg_contact_not_updated));
                }
            }
        }

        if (mContactLoaded == null && hasSaved) {
            Message.showLongMessage(getApplicationContext(), message.toString());
            finish();
        } else {
            Message.showLongMessage(v, message.toString());
        }
    }

    private boolean checkFields(Contact contact) {
        return contact.getName().isEmpty() || contact.getEmail().isEmpty();
    }

    private void removeContact() {
        String message = getString(R.string.msg_contact_not_deleted);
        if (mContactLoaded != null) {
            if (contatosDB.deleteContactById(mContactLoaded.getId())) {
                message = getString(R.string.msg_contact_deleted);
            }
            Message.showLongMessage(getApplicationContext(), message);
            finish();
        }
    }
}