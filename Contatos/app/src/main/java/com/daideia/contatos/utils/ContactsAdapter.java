package com.daideia.contatos.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.daideia.contatos.R;
import com.daideia.contatos.models.Contact;

import java.util.List;

public class ContactsAdapter extends ArrayAdapter<Contact> {
    private Contact mContact;
    private TextView mTxtContact;

    public ContactsAdapter(Context ctx, List<Contact> contacts) {
        super(ctx, 0, contacts);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        mContact = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.item_contact, null);
        }

        mTxtContact = (TextView) convertView.findViewById(R.id.lblContactName);
        mTxtContact.setText(mContact.getName());

        return convertView;
    }
}