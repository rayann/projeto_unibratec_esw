package com.daideia.contatos.database.tables;

import android.provider.BaseColumns;

public interface ContactsTB extends BaseColumns {
    String TABLE_NAME = "Contacts";

    String ID         = "ID";
    String NAME       = "Name";
    String PHONE      = "Phone";
    String CELLPHONE  = "Cellphone";
    String EMAIL      = "Email";

    String createTableContactsQuery = "CREATE TABLE IF NOT EXISTS " +
            ContactsTB.TABLE_NAME + " (" +
            ContactsTB.ID         + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ContactsTB.NAME       + " TEXT NOT NULL, " +
            ContactsTB.PHONE      + " TEXT NOT NULL, " +
            ContactsTB.CELLPHONE  + " TEXT NOT NULL, " +
            ContactsTB.EMAIL      + " TEXT NOT NULL)";

    String deleteTableContactsQuery = "DROP TABLE IF EXISTS " + ContactsTB.TABLE_NAME;
}