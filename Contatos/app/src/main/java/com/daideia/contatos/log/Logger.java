package com.daideia.contatos.log;

import android.util.Log;

public class Logger {

    public static final String TAG = "---APP CONTATOS---";
    public static final int DEBUG = 0;
    public static final int INFO = 1;
    public static final int WARNING = 2;
    public static final int ERROR = 3;
    public static final int VERBOSE = 4;

    public static void log(int type, final String msg) {
        switch (type) {
            case DEBUG:
                Log.d(TAG, msg);
                break;
            case INFO:
                Log.i(TAG, msg);
                break;
            case WARNING:
                Log.w(TAG, msg);
                break;
            case ERROR:
                Log.e(TAG, msg);
                break;
            case VERBOSE:
                Log.v(TAG, msg);
                break;
        }
    }
}