package com.daideia.contatos.models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Contact implements Serializable {

    private int id;
    private String name, phone, cellphone, email;

    public Contact(int id, String name, String phone, String cellphone, String email) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.cellphone = cellphone;
        this.email = email;
    }

    public Contact(String name, String phone, String cellphone, String email) {
        this.name = name;
        this.phone = phone;
        this.cellphone = cellphone;
        this.email = email;
    }

    public Contact() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
